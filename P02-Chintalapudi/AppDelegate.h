//
//  AppDelegate.h
//  P02-Chintalapudi
//
//  Created by MaNi on 1/28/17.
//  Copyright © 2017 MaNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

