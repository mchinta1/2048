//
//  ViewController.h
//  P02-Chintalapudi
//
//  Created by MaNi on 1/28/17.
//  Copyright © 2017 MaNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong,nonatomic) IBOutletCollection(UILabel) NSArray
*labelCollection;

@property(strong, nonatomic) IBOutlet UITextField *score;

-(IBAction)NewGame:(UIButton * )sender;

@property(strong, nonatomic) IBOutlet UITextField *HighScore;


@end

