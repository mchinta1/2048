//
//  ViewController.m
//  P02-Chintalapudi
//
//  Created by MaNi on 1/28/17.
//  Copyright © 2017 MaNi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

@synthesize score,HighScore;

NSString *Numbers_Array[4][4] = {@""};

int count = 0, possible_move = 0, vaccant_tile = 0;

NSInteger lastHighScore;

NSInteger vaccant_array[16] = {0};

bool paired_array[4] = {false};

bool isGameOver;

bool Is2048 = NO;

@synthesize labelCollection;

- (void)viewDidLoad {
    
    lastHighScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"high_score"];

    [HighScore setText:[NSString stringWithFormat:@"%ld",lastHighScore]];
    
    count = 0;
    
    [super viewDidLoad];
    
    [score setText:@"0"];
    
    int rand = arc4random_uniform(15);
    
    ((UILabel *)[labelCollection objectAtIndex:rand]).text = @"2";

   Numbers_Array[rand/4][rand%4] = @"2";
    
 //  [self UpdateArray];
    [self setupSwipeControls];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)NewGame:(UIButton * )sender
{
    count = 0;
    
     count = 0, possible_move = 0, vaccant_tile = 0;
    
    int i =0;
    UILabel *label;
    
    for (label in labelCollection)
    {
        label.text = @"";
        vaccant_array[i] = 0;
        i++;
    }
    
    [self PairedArrayinit];
    
     isGameOver = false;
    
     Is2048 = NO;
    
    NSString *Numbers_Array[4][4] = {@""};
    
    [score setText:@"0"];
    
    int rand = arc4random_uniform(15);
    
    ((UILabel *)[labelCollection objectAtIndex:rand]).text = @"2";
    
    Numbers_Array[rand/4][rand%4] = @"2";
    [self UpdateArray];
    
}

//Move Right
-(void)swipeRight
{
    long val = 0;
    possible_move = 0;
    for(int i = 0; i<=3; i++)
    {
        [self PairedArrayinit];
        for(int j=3; j>0; j--)
        {
            if(Numbers_Array[i][j].length !=0 && Numbers_Array[i][j-1].length !=0 && [Numbers_Array[i][j-1] isEqualToString:Numbers_Array[i][j]] && !paired_array[j] && !paired_array[j-1])
            {
                val = 2 * [Numbers_Array[i][j-1] integerValue];
                [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                Numbers_Array[i][j] =  [NSString stringWithFormat:@"%ld", val];
                Numbers_Array[i][j-1] = @"";
                possible_move++;
                paired_array[j] = YES;
            }
           else if(Numbers_Array[i][j].length == 0 && Numbers_Array[i][j-1].length !=0 )
            {
                Numbers_Array[i][j] = Numbers_Array[i][j-1];
                Numbers_Array[i][j-1] = @"";
                possible_move++;
                int col = j +1, k = j;
                    while( col <= 3 &&  k <= 3)
                    {
                        if(Numbers_Array[i][col].length == 0)
                        {
                        Numbers_Array[i][col] =  Numbers_Array[i][k];
                        Numbers_Array[i][k] = @"";
                        possible_move++;
                       
                        }
                        else if([Numbers_Array[i][col] isEqualToString: Numbers_Array[i][k]])
                        {
                            if(!paired_array[col] && !paired_array[k])
                            {
                            val = 2 * [Numbers_Array[i][col] integerValue];
                            [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                            
                            Numbers_Array[i][col] =  [NSString stringWithFormat:@"%ld", val];
                            Numbers_Array[i][k] = @"";
                            possible_move++;
                            paired_array[col] = YES;

                            if (col == 2) {
                                break;
                            }
                            }
                            
                        }
                    col++;
                    k++;
                    }
                
                }
            NSLog(@"\n%@\t%@\t%@\t%@",Numbers_Array[i][0],Numbers_Array[i][1],Numbers_Array[i][2],Numbers_Array[i][3]);
        }
    }
    [self UpdateLableCollection];
    
   }


//Move Left
-(void)swipeLeft
{

    long val = 0;
    possible_move = 0;
    for(int i = 0; i<=3; i++)
    {
        [self PairedArrayinit];
        for(int j=0; j<3; j++)
        {
            if(Numbers_Array[i][j].length !=0 && Numbers_Array[i][j+1].length !=0 && [Numbers_Array[i][j+1] isEqualToString:Numbers_Array[i][j]] && !paired_array[j] && !paired_array[j+1])
            {
                val = 2 * [Numbers_Array[i][j+1] integerValue];
                [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                Numbers_Array[i][j] =  [NSString stringWithFormat:@"%ld", val];
                Numbers_Array[i][j+1] = @"";
                possible_move++;
                paired_array[j]= YES;
            }
            else if(Numbers_Array[i][j].length == 0 && Numbers_Array[i][j+1].length !=0 )
            {
                Numbers_Array[i][j] = Numbers_Array[i][j+1];
                Numbers_Array[i][j+1] = @"";
                possible_move++;
                int col = j-1, k = j;
                while( col >= 0 &&  k >= 0)
                {
                    if(Numbers_Array[i][col].length == 0)
                    {
                        Numbers_Array[i][col] =  Numbers_Array[i][k];
                        Numbers_Array[i][k] = @"";
                        possible_move++;
                        
                    }
                    else if([Numbers_Array[i][col] isEqualToString: Numbers_Array[i][k]])
                    {
                       if(!paired_array[col] && !paired_array[k])
                       {
                        val = 2 * [Numbers_Array[i][col] integerValue];
                        [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                        
                        Numbers_Array[i][col] =  [NSString stringWithFormat:@"%ld", val];
                        Numbers_Array[i][k] = @"";
                        possible_move++;
                        paired_array[col] = YES;

                        if (col == 1) {
                            break;
                        }
                       }
                        
                    }
                    col--;
                    k--;
                }
                
                
            }
            NSLog(@"\n%@\t%@\t%@\t%@",Numbers_Array[i][0],Numbers_Array[i][1],Numbers_Array[i][2],Numbers_Array[i][3]);
        }
    }
    vaccant_tile = 0;
    [self UpdateLableCollection];
    
}


//Move Up
-(void)swipeUp
{
    long val = 0;
    possible_move = 0;
    for(int j = 0; j<=3; j++)
    {
        [self PairedArrayinit];
        for(int i=0; i<3; i++)
        {
            if(Numbers_Array[i][j].length !=0 && Numbers_Array[i+1][j].length !=0 && [Numbers_Array[i+1][j] isEqualToString:Numbers_Array[i][j]]&& !paired_array[i] && !paired_array[i+1])
            {
                val = 2 * [Numbers_Array[i][j] integerValue];
                [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                Numbers_Array[i][j] =  [NSString stringWithFormat:@"%ld", val];
                Numbers_Array[i+1][j] = @"";
                paired_array[i] = YES;
                possible_move++;
            }
            else if(Numbers_Array[i][j].length == 0 && Numbers_Array[i+1][j].length !=0 )
            {
                Numbers_Array[i][j] = Numbers_Array[i+1][j];
                Numbers_Array[i+1][j] = @"";
                possible_move++;
                int row = i-1, k = i;
                while( row >= 0 &&  k >= 0)
                {
                    if(Numbers_Array[row][j].length == 0)
                    {
                        Numbers_Array[row][j] =  Numbers_Array[k][j];
                        Numbers_Array[k][j] = @"";
                        possible_move++;
                        
                    }
                    else if([Numbers_Array[row][j] isEqualToString: Numbers_Array[k][j]])
                    {
                        if(!paired_array[row] && !paired_array[k])
                        {
                        val = 2 * [Numbers_Array[row][j] integerValue];
                        [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                        
                        Numbers_Array[row][j] =  [NSString stringWithFormat:@"%ld", val];
                        Numbers_Array[k][j] = @"";
                        possible_move++;
                        paired_array[row] = YES;
                        if (row == 1) {
                            break;
                        }
                    }
                        
                    }
                    row--;
                    k--;
                }
                
                
            }
            NSLog(@"\n%@\t%@\t%@\t%@",Numbers_Array[0][j],Numbers_Array[1][j],Numbers_Array[2][j],Numbers_Array[3][j]);
        }
    }
    vaccant_tile = 0;
    [self UpdateLableCollection];
    
}

-(void)swipeDown
{
        long val = 0;
   
        possible_move = 0;
        for(int j = 0; j<=3; j++)
        {
             [self PairedArrayinit];
            
            for(int i=3; i>0; i--)
            {
                if(Numbers_Array[i][j].length !=0 && Numbers_Array[i-1][j].length !=0 && [Numbers_Array[i-1][j] isEqualToString:Numbers_Array[i][j]]&& !paired_array[i] && !paired_array[i-1])
                {
                    val = 2 * [Numbers_Array[i][j] integerValue];
                    [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                    Numbers_Array[i][j] =  [NSString stringWithFormat:@"%ld", val];
                    Numbers_Array[i-1][j] = @"";
                    possible_move++;
                    paired_array[i] = YES;
                    
                }
                else if(Numbers_Array[i][j].length == 0 && Numbers_Array[i-1][j].length !=0 )
                {
                    Numbers_Array[i][j] = Numbers_Array[i-1][j];
                    Numbers_Array[i-1][j] = @"";
                    possible_move++;
                    int row = i+1, k = i;
                    while( row <= 3 &&  k >= 0)
                    {
                        
                        if(Numbers_Array[row][j].length == 0)
                        {
                            Numbers_Array[row][j] =  Numbers_Array[k][j];
                            Numbers_Array[k][j] = @"";
                            possible_move++;
                            
                        }
                        else if([Numbers_Array[row][j] isEqualToString: Numbers_Array[k][j]])
                        {
                           if(!paired_array[row] && !paired_array[k]){
                            val = 2 * [Numbers_Array[row][j] integerValue];
                            [score setText:[NSString stringWithFormat:@"%ld", [score.text integerValue] + val]];
                            
                            Numbers_Array[row][j] =  [NSString stringWithFormat:@"%ld", val];
                            Numbers_Array[k][j] = @"";
                            possible_move++;
                            paired_array[row] = YES;
                            if (row == 2) {
                                break;
                            }
                            
                            }
                        }
                        row++;
                        k++;
                    }
                    
                    
                }
                NSLog(@"\n%@\t%@\t%@\t%@",Numbers_Array[0][j],Numbers_Array[1][j],Numbers_Array[2][j],Numbers_Array[3][j]);
            }
        }
        vaccant_tile = 0;
        [self UpdateLableCollection];
        
}

-(void)PairedArrayinit
{
    paired_array[0] = NO;
    paired_array[1] = NO;
    paired_array[2] = NO;
    paired_array[3] = NO;
    
}

-(void)GameOverAlert
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Game Over"
                                 message: (Is2048) ? @"You Won" : @"You Lose"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok_button = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    [alert addAction:ok_button];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)UpdateArray
{
    count = 0;
    for(int i = 0; i<=3; i++)
    {
        for(int j = 0; j<= 3; j++ )
        {
            Numbers_Array[i][j] = ((UILabel *)[labelCollection objectAtIndex:count]).text;
            count++;
        }
        
    }
    
}

-(void)UpdateLableCollection
{
    
    count = 0;
    int k = 0;
    Is2048 = NO;
    vaccant_tile = 0;
    for(int i = 0; i<=3; i++)
    {
        for(int j = 0; j<= 3; j++ )
        {
            ((UILabel *)[labelCollection objectAtIndex:count]).text =   Numbers_Array[i][j];

            if([Numbers_Array[i][j] integerValue] == 2048)
                Is2048 = YES;
            if(Numbers_Array[i][j].length == 0)
            {
                vaccant_tile++;
                vaccant_array[k] = i * 4 + j;
                k++;
            }
            
            count++;
        }
        
    }
    if(vaccant_tile > 0 && possible_move > 0 )
    {
        [self GenerateNumber];
        
        vaccant_tile--;

    }
    
    if(([self ISGameOver] && vaccant_tile == 0) || Is2048)
    {
        [self GameOverAlert];
    }
}

-(long)GenerateNumber
{
    long tile_position = 0;
    tile_position = arc4random_uniform(vaccant_tile);
    tile_position = vaccant_array[tile_position];
    
    NSLog(@"tile position: %lu\t%lu",tile_position,(((UILabel *)[labelCollection objectAtIndex: tile_position]).text.length));
    while (((UILabel *)[labelCollection objectAtIndex: tile_position]).text.length !=0 )
    {
        tile_position = arc4random_uniform(vaccant_tile);
        tile_position = vaccant_array[tile_position];
        
    }
    NSLog(@"\n%lu",tile_position);
    [self LabelAnimate:tile_position];
    Numbers_Array[(int)tile_position/4][tile_position%4] = @"2";
    [self CheckHighScore];
    
    return tile_position;
}

-(void)LabelAnimate:(long) index
{
    UILabel *label = [labelCollection objectAtIndex:index];
    
    [UIView transitionWithView:label
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        label.text = @"2";
                    } completion:nil];
}

- (void)setupSwipeControls
{
        UISwipeGestureRecognizer *upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(swipeUp)];
        upSwipe.numberOfTouchesRequired = 1;
        upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer:upSwipe];
        
        UISwipeGestureRecognizer *downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(swipeDown)];
        downSwipe.numberOfTouchesRequired = 1;
        downSwipe.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer:downSwipe];
        
        UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(swipeLeft)];
        leftSwipe.numberOfTouchesRequired = 1;
        leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:leftSwipe];
        
        UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                         action:@selector(swipeRight)];
        rightSwipe.numberOfTouchesRequired = 1;
        rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:rightSwipe];
}

-(bool)ISGameOver
{
    
    int i = 0, merge = 0;
    
    for(i = 0; i<= 3; i++)
    {
        for(int j = 0; j<= 3; j++)
        {
    if(i+1 <= 3 &&  merge == 0 && ((i+1)*4+j) >= 0 && ((i+1)*4+j) <= 15 && ((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text.length !=0 )
        {
            if(((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text ==  ((UILabel *) [labelCollection objectAtIndex:((i+1)*4+j)]).text)
               
            {
                merge++;
            }
            
        }
            if( j+1 <= 3  &&(i*4+j+1) >= 0 &&  (i*4+j+1) <= 15 &&  merge == 0 && ((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text.length !=0 )
            {
                if(((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text ==  ((UILabel *) [labelCollection objectAtIndex:(i*4+j+1)]).text )
                {
                    merge++;
                }
                
            }
            if(j-1 >= 0 && (i*4+j-1) >= 0 && (i*4+j-1) <= 15 && merge == 0 && ((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text.length !=0 )
            {
                if(((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text ==  ((UILabel *) [labelCollection objectAtIndex:(i*4+j-1)]).text)
                {
                    merge++;
                }
                
            }
            if(i-1 >=0 && ((i-1)*4+j) > 0 && ((i-1)*4+j) <= 15  &&  merge == 0 && ((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text.length !=0 )
            {
                if(((UILabel *) [labelCollection objectAtIndex:(i*4+j)]).text ==  ((UILabel *) [labelCollection objectAtIndex:((i-1)*4+j)]).text)
                {
                    merge++;
                }
                
            }

    
       }
        }
    
    if(merge > 0)
        isGameOver = NO;
    else
    {
        isGameOver = YES;
      if(lastHighScore <= [score.text integerValue])
        [[NSUserDefaults standardUserDefaults] setInteger:[score.text integerValue] forKey:@"high_score"];
    }
    return isGameOver;

}

-(void)CheckHighScore
{

    if(lastHighScore < [score.text integerValue] || lastHighScore == 0 )
    {
        //lastHighScore = [score.text integerValue];
        
        [HighScore setText:[NSString stringWithFormat:@"%ld",[score.text integerValue]]];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[score.text integerValue] forKey:@"high_score"];
    }
    
}

@end
